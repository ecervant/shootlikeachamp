#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>

const int SCREEN_WIDTH = 640; // dimension for screen width
const int SCREEN_HEIGHT = 480; // dimension for screen height

class LTexture // class for texture
{
	public:
		
		LTexture(); // default constructor

		~LTexture(); // deconstructor

		bool loadFromFile( std::string path ); // imports the image

		void free(); // frees the memory/ deallocator

		void setColor( Uint8 red, Uint8 green, Uint8 blue ); // for color

		//from the SDL_image library:

		void setBlendMode( SDL_BlendMode blending ); // for blending

		void setAlpha( Uint8 alpha ); // alpha modulation
		
		void render( int x, int y, SDL_Rect* clip = NULL ); // rendering @ specified point

		int getWidth(); // returns image width
		int getHeight(); // returns image height

	private:
		SDL_Texture* mTexture; // texture

		int mWidth; // image width
		int mHeight; // image height
};

bool init(); //initializes everyhting aka creates window

bool loadMedia(); //checks to see if image can be loaded

void close(); // shutsdown SDL

SDL_Window* gWindow = NULL; // window to render

SDL_Renderer* gRenderer = NULL; // window renderer

const int WALKING_ANIMATION_FRAMES = 5; // animation
SDL_Rect gSpriteClips[ WALKING_ANIMATION_FRAMES ];
LTexture gSpriteSheetTexture;


LTexture::LTexture() // initialize
{
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

LTexture::~LTexture() // frees memory
{
	free();
}

bool LTexture::loadFromFile( std::string path )
{
	free(); // clears any texture that is already there

	SDL_Texture* newTexture = NULL; // texture

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() ); //loading image
	
	if( loadedSurface == NULL )
	{
		printf( "Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError() );
	}
	else
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			printf( "Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError() );
		}
		else
		{
			mWidth = loadedSurface->w; // image width
			mHeight = loadedSurface->h; // image height
		}

		SDL_FreeSurface( loadedSurface ); // clears the surface for new clip
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}

void LTexture::free()
{
	//Free texture if it exists
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

// from the SDL_image library : ...

void LTexture::setColor( Uint8 red, Uint8 green, Uint8 blue )
{
	SDL_SetTextureColorMod( mTexture, red, green, blue );
}

void LTexture::setBlendMode( SDL_BlendMode blending )
{
	SDL_SetTextureBlendMode( mTexture, blending );
}
		
void LTexture::setAlpha( Uint8 alpha )
{
	SDL_SetTextureAlphaMod( mTexture, alpha );
}

void LTexture::render( int x, int y, SDL_Rect* clip )
{
	SDL_Rect renderQuad = { x, y, mWidth, mHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, mTexture, clip, &renderQuad ); //renders to screen
}

int LTexture::getWidth() // returns image width
{
	return mWidth;
}

int LTexture::getHeight() // returns image height
{
	return mHeight;
}

bool init() // initializes SDL
{
	bool success = true;

	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		gWindow = SDL_CreateWindow( "SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}
		else
		{
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia() // determines if the image was loaded successfully
{
	bool success = true;

	if( !gSpriteSheetTexture.loadFromFile( "bob.png" ) ) // sprite sheet
	{
		printf( "Failed to load walking animation texture!\n" ); // failed load
		success = false;
	}
	else
	{
		// image seperated into sprite sheets
		gSpriteClips[ 0 ].x =   0;
		gSpriteClips[ 0 ].y =   0;
		gSpriteClips[ 0 ].w = 128;
		gSpriteClips[ 0 ].h = 225;

		gSpriteClips[ 1 ].x = 128;
		gSpriteClips[ 1 ].y =   0;
		gSpriteClips[ 1 ].w = 128;
		gSpriteClips[ 1 ].h = 225;
		
		gSpriteClips[ 2 ].x = 256;
		gSpriteClips[ 2 ].y =   0;
		gSpriteClips[ 2 ].w = 128;
		gSpriteClips[ 2 ].h = 225;

		gSpriteClips[ 3 ].x = 384;
		gSpriteClips[ 3 ].y =   0;
		gSpriteClips[ 3 ].w = 128;
		gSpriteClips[ 3 ].h = 225;

		gSpriteClips[ 4 ].x = 512;
		gSpriteClips[ 4 ].y =   0;
		gSpriteClips[ 4 ].w = 128;
		gSpriteClips[ 4 ].h = 225;
	}
	
	return success;
}

void close() // shuts everything down
{
	gSpriteSheetTexture.free();

	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	IMG_Quit();
	SDL_Quit();
}

int main( int argc, char* args[] )
{
	if( !init() ) // initializes SDL
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		if( !loadMedia() ) // determines if image was loaded successfully
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			bool quit = false;

			SDL_Event e;

			int frame = 0; // starts at the first clip

			while( !quit )
			{
				while( SDL_PollEvent( &e ) != 0 )
				{
					if( e.type == SDL_QUIT ) // quits if key pressed
					{
						quit = true;
					}
				}

				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF ); // clears screen
				SDL_RenderClear( gRenderer );

				SDL_Rect* currentClip = &gSpriteClips[ frame / 5 ];
				gSpriteSheetTexture.render( ( SCREEN_WIDTH - currentClip->w ) / 2, ( SCREEN_HEIGHT - currentClip->h ) / 2, currentClip );

				SDL_RenderPresent( gRenderer );

				++frame; // moves onto next frame while there is no user input

				if( frame / 5 >= WALKING_ANIMATION_FRAMES ) // starts clips all over again
				{
					frame = 0;
				}
			}
		}
	}

	close(); // closes SDL

	return 0;
}