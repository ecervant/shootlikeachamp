#include <SDL2/SDL.h>
#include <stdio.h>

int main(int argc, char ** argv)
{
    // variables that will keep the window open, take in all the events, and position the image
 
    bool quit = false;
    SDL_Event event;
    int x = 250;
    int y = 250;
 
    // initialize the window for SDL
 
    SDL_Init(SDL_INIT_VIDEO);
    SDL_Window * window = SDL_CreateWindow("Move an Image with Keyboard Events",
        SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, 0);

    //structure that handles the rendering
    SDL_Renderer * renderer = SDL_CreateRenderer(window, -1, 0);
 
    SDL_Surface * image = SDL_LoadBMP("sprite.bmp");
    //texture being rendered
    SDL_Texture * texture = SDL_CreateTextureFromSurface(renderer,
        image);

    SDL_FreeSurface(image);
    // sets the screen to white 
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
 
    // handle events
 
    while (!quit)
    {
        SDL_Delay(1); // so that the image doesn't move as quickly
        SDL_PollEvent(&event); // attaches all the events to the variable created
 
        switch (event.type){ // navigate through the events
        case SDL_QUIT: // if the user clicks the X
            quit = true; // exit condition is true
            break; // break out of switch case
        case SDL_KEYDOWN: // for keyboard movement
            switch (event.key.keysym.sym) // go through arrows
            {
                case SDLK_LEFT:  x--; break;
                case SDLK_RIGHT: x++; break;
                case SDLK_UP:    y--; break;
                case SDLK_DOWN:  y++; break;
            }
            break;
	}
 
        SDL_Rect dstrect = { x, y, 100, 100 }; // position, width, height of image
 
        SDL_RenderClear(renderer); // clears renderign target with the set color (white)
        SDL_RenderCopy(renderer, texture, NULL, &dstrect); // copies texture to the rendering target
        SDL_RenderPresent(renderer); // draws it in the window
    }
 
    // close SDL
 
    SDL_DestroyTexture(texture);
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
 
    return 0;
}
