// Squirrel class
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>


//Screen dimension constants
const int SCREEN_WIDTH = 770;
const int SCREEN_HEIGHT = 770;

bool init();
bool loadMedia();
void close();

class Squirrel
{
public:
	Squirrel();
	~Squirrel();

	bool loadFromFile(std::string path);
	void free();

	void render( int x, int y, SDL_Rect* clip = NULL );

	int getWidth();
	int getHeight();
	int getXPos();
	int getYPos();
	// void setXPos();
	int getRandom();
	void setYPos(int y);

private:
	SDL_Texture* sTexture;

	int sWidth;
	int sHeight;
	int squirrel_xpos;
	int squirrel_ypos;
	int random_y;
};


SDL_Window* gWindow = NULL; // window to render
SDL_Renderer* gRenderer = NULL; // window renderer

const int SQUIRREL_ANIMATION_FRAMES = 4; // animation
SDL_Rect gSquirrelClips[ SQUIRREL_ANIMATION_FRAMES ];
std::vector<Squirrel> gSquirrelSheetTexture; // instanciation of class


Squirrel::Squirrel()
{

	sWidth = 0;
	sHeight = 0;
	sTexture = NULL;
	squirrel_xpos = rand() % 262 + 288; //between 288 and 550
	std::cout << squirrel_xpos << std::endl;
	squirrel_ypos = 0;
	random_y = rand() %200;
}

Squirrel::~Squirrel()
{

	free();
}

bool Squirrel::loadFromFile( std::string path )
{
	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );

	if (loadedSurface == NULL)
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;

	else
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);

		if (newTexture == NULL)
			std::cout << "Unable to create texture from " << path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		else
		{
			sWidth = loadedSurface->w;
			sHeight = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}

	sTexture = newTexture;
	return sTexture != NULL;
}

void Squirrel::free()
{
	if (sTexture != NULL)
	{
		SDL_DestroyTexture(sTexture);
		sTexture = NULL;
		sWidth = 0;
		sHeight = 0;
	}
}

void Squirrel::render(int x, int y, SDL_Rect* clip)
{
	SDL_Rect renderQuad = { x, y, sWidth, sHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, sTexture, clip, &renderQuad ); 
}

int Squirrel::getWidth()
{
	return sWidth;
}

int Squirrel::getHeight()
{
	return sHeight;
}

int Squirrel::getXPos()
{
	return squirrel_xpos;
}

int Squirrel::getRandom(){
	return random_y;
}

int Squirrel::getYPos()
{
	return squirrel_ypos;
}

// void Squirrel::setXPos()
// {
// 	squirrel_xpos =  SCREEN_WIDTH / 2 ; // const
// }

void Squirrel::setYPos(int y)
{
	squirrel_ypos = ( getHeight() / 10 ) + y- random_y;
}

bool init()
{
	bool success = true;

	if (SDL_Init( SDL_INIT_VIDEO ) < 0)
	{
		std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
		success = false;
	}

	else
	{
		if ( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
			std::cout << "Warning: Linear texture filtering not enabled!" << std::endl;
		gWindow = SDL_CreateWindow( "Squirrel", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if( gWindow == NULL )
		{
			std::cout << "Window could not be created! SDL Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if (gRenderer == NULL)
			{
				std::cout << "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					std::cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
					success = false;
				}
			}
		}
	}
	return success;
}

bool loadMedia() // determines if the image was loaded successfully
{
	bool success = true;

	for (int h = 0; h < gSquirrelSheetTexture.size();h++){

	if( !gSquirrelSheetTexture[h].loadFromFile( "squirrel.png" ) ) // sprite sheet
	{
		std::cout << "Failed to load walking animation texture!" << std::endl; // failed load
		success = false;
	}
	else
	{
		// image seperated into sprite sheets
		gSquirrelClips[ 0 ].x =  0;
		gSquirrelClips[ 0 ].y =  0;
		gSquirrelClips[ 0 ].w = 34;
		gSquirrelClips[ 0 ].h = 43;

		gSquirrelClips[ 1 ].x = 34;
		gSquirrelClips[ 1 ].y =  0;
		gSquirrelClips[ 1 ].w = 34;
		gSquirrelClips[ 1 ].h = 43;
		
		gSquirrelClips[ 2 ].x = 68;
		gSquirrelClips[ 2 ].y = 0;
		gSquirrelClips[ 2 ].w = 34;
		gSquirrelClips[ 2 ].h = 43;

		gSquirrelClips[ 3 ].x = 68;
		gSquirrelClips[ 3 ].y = 0;
		gSquirrelClips[ 3 ].w = 34;
		gSquirrelClips[ 3 ].h = 43;

	}
	}
	return success;
}

void close()
{
	for (int i = 0; i < gSquirrelSheetTexture.size();i++){
		gSquirrelSheetTexture[i].free();
	}

	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	IMG_Quit();
	SDL_Quit();
}

int main( int argc, char* args[] )
{
	srand(time(0));
	int y = 0; // change in the y

	for (int j = 0; j<4;j++){
		Squirrel temp;
		gSquirrelSheetTexture.push_back(temp);
	}

	if( !init() ) // initializes SDL
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		if( !loadMedia() ) // determines if image was loaded successfully
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			bool quit = false;

			SDL_Event e;

			int frame = 0; // starts at the first clip

			while( !quit )
			{
				SDL_Delay(1);
				SDL_PollEvent( &e );

				switch(e.type)
				{
					case SDL_QUIT:
						quit = true;
						break;
					
				}
				
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF ); // clears screen
				SDL_RenderClear( gRenderer );

				SDL_Rect* currentClip = &gSquirrelClips[ frame / 4 ];
				++y;

				for (int k=0; k<gSquirrelSheetTexture.size();k++){
					gSquirrelSheetTexture[k].setYPos(y);
				}
				// gSquirrelSheetTexture.setXPos();

				for ( int k=0; k< gSquirrelSheetTexture.size(); k++){
					// int sub = gSquirrelSheetTexture[k].getRandom();
					if (gSquirrelSheetTexture[k].getYPos() >= 730)
					{
						// y = 0;
						gSquirrelSheetTexture[k].setYPos(950);

					}
				}
				for ( int k=0; k<gSquirrelSheetTexture.size(); k++){
					gSquirrelSheetTexture[k].render( gSquirrelSheetTexture[k].getXPos() , gSquirrelSheetTexture[k].getYPos() , currentClip );
					std::cout << k << std::endl;
					// std::cout << gSquirrelSheetTexture[k].getYPos() << std::endl;
					//gSquirrelSheetTexture[k].render( gSquirrelSheetTexture[k].getXPos() +20, gSquirrelSheetTexture[k].getYPos() +50, currentClip );
				}
				SDL_RenderPresent( gRenderer );

				++frame; // moves onto next frame while there is no user input

				if( frame / 4 >= SQUIRREL_ANIMATION_FRAMES ) // starts clips all over again
				{
					frame = 0;
				}
			}
		}
	}

	close();

	return 0;
}