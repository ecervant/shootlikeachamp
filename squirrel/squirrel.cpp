// Squirrel class
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
// vector library


//Screen dimension constants
const int SCREEN_WIDTH = 770;
const int SCREEN_HEIGHT = 770;

bool init();
bool loadMedia();
void close();

class Squirrel
{
public:
	Squirrel();
	~Squirrel();

	bool loadFromFile(std::string path);
	void free();

	void render( int x, int y, SDL_Rect* clip = NULL );

	int getWidth();
	int getHeight();

private:
	SDL_Texture* sTexture;

	int sWidth;
	int sHeight;
	// vector of enemies
};

SDL_Window* gWindow = NULL; // window to render
SDL_Renderer* gRenderer = NULL; // window renderer

const int SQUIRREL_ANIMATION_FRAMES = 4; // animation
SDL_Rect gSquirrelClips[ SQUIRREL_ANIMATION_FRAMES ];
Squirrel gSquirrelSheetTexture; // instanciation of class


Squirrel::Squirrel()
{
	sWidth = 0;
	sHeight = 0;
	sTexture = NULL;
}

Squirrel::~Squirrel()
{
	free();
}

bool Squirrel::loadFromFile( std::string path )
{
	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );

	if (loadedSurface == NULL)
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;

	else
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);

		if (newTexture == NULL)
			std::cout << "Unable to create texture from " << path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		else
		{
			sWidth = loadedSurface->w;
			sHeight = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}

	sTexture = newTexture;
	return sTexture != NULL;
}

void Squirrel::free()
{
	if (sTexture != NULL)
	{
		SDL_DestroyTexture(sTexture);
		sTexture = NULL;
		sWidth = 0;
		sHeight = 0;
	}
}

void Squirrel::render(int x, int y, SDL_Rect* clip)
{
	SDL_Rect renderQuad = { x, y, sWidth, sHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, sTexture, clip, &renderQuad ); 
}

int Squirrel::getWidth()
{
	return sWidth;
}

int Squirrel::getHeight()
{
	return sHeight;
}

bool init()
{
	bool success = true;

	if (SDL_Init( SDL_INIT_VIDEO ) < 0)
	{
		std::cout << "SDL could not initialize! SDL Error: " << SDL_GetError() << std::endl;
		success = false;
	}

	else
	{
		if ( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
			std::cout << "Warning: Linear texture filtering not enabled!" << std::endl;
		gWindow = SDL_CreateWindow( "Squirrel", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if( gWindow == NULL )
		{
			std::cout << "Window could not be created! SDL Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			if (gRenderer == NULL)
			{
				std::cout << "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					std::cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
					success = false;
				}
			}
		}
	}
	return success;
}

bool loadMedia() // determines if the image was loaded successfully
{
	bool success = true;

	if( !gSquirrelSheetTexture.loadFromFile( "squirrel.png" ) ) // sprite sheet
	{
		std::cout << "Failed to load walking animation texture!" << std::endl; // failed load
		success = false;
	}
	else
	{
		// image seperated into sprite sheets
		gSquirrelClips[ 0 ].x =  0;
		gSquirrelClips[ 0 ].y =  0;
		gSquirrelClips[ 0 ].w = 34;
		gSquirrelClips[ 0 ].h = 43;

		gSquirrelClips[ 1 ].x = 34;
		gSquirrelClips[ 1 ].y =  0;
		gSquirrelClips[ 1 ].w = 34;
		gSquirrelClips[ 1 ].h = 43;
		
		gSquirrelClips[ 2 ].x = 68;
		gSquirrelClips[ 2 ].y = 0;
		gSquirrelClips[ 2 ].w = 34;
		gSquirrelClips[ 2 ].h = 43;

		gSquirrelClips[ 3 ].x = 68;
		gSquirrelClips[ 3 ].y = 0;
		gSquirrelClips[ 3 ].w = 34;
		gSquirrelClips[ 3 ].h = 43;

	}
	
	return success;
}

void close()
{
	gSquirrelSheetTexture.free();

	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	IMG_Quit();
	SDL_Quit();
}

int main( int argc, char* args[] )
{
	int x = 0; // change in the x
	int y = 0; // change in the y
	int xpos_squirrel;
	int ypos_squirrel;
	if( !init() ) // initializes SDL
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		if( !loadMedia() ) // determines if image was loaded successfully
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			bool quit = false;

			SDL_Event e;

			int frame = 0; // starts at the first clip

			while( !quit )
			{
				SDL_Delay(1);
				SDL_PollEvent( &e );

				switch(e.type)
				{
					case SDL_QUIT:
						quit = true;
						break;
					// case SDL_KEYDOWN:
					// 	switch(e.key.keysym.sym)
					// 	{
					// 		case SDLK_LEFT:  x--; break;
		   //              	case SDLK_RIGHT: x++; break;
			  //               case SDLK_UP:    y--; break;
					// 	}
					// 	break;
				}
				
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF ); // clears screen
				SDL_RenderClear( gRenderer );

				SDL_Rect* currentClip = &gSquirrelClips[ frame / 4 ];
				++y;
				xpos_squirrel = ( SCREEN_WIDTH - currentClip->w ) / 2 ;
				ypos_squirrel = ( currentClip->h / 7 ) + y;
				
				if (ypos_squirrel >= 730)
					y = 0;
				// gSquirrelSheetTexture.render( (( SCREEN_WIDTH - currentClip->w ) / 2) + x, (9.75 * ( SCREEN_HEIGHT - currentClip->h ) / 10) + y , currentClip );
				gSquirrelSheetTexture.render( xpos_squirrel , ypos_squirrel , currentClip );

				SDL_RenderPresent( gRenderer );

				++frame; // moves onto next frame while there is no user input

				if( frame / 4 >= SQUIRREL_ANIMATION_FRAMES ) // starts clips all over again
				{
					frame = 0;
				}
			}
		}
	}

	close(); // closes SDL

	return 0;
}