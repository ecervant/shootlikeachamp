// Squirrel class
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>


class Squirrel
{
public:
	Squirrel();
	~Squirrel();

	bool loadFromFile(std::string path);
	void free();

	void render( int x, int y, SDL_Rect* clip = NULL );

	int getWidth();
	int getHeight();
	int getXPos();
	int getYPos();
	// void setXPos();
	int getRandom();
	void setYPos(int y);

private:
	SDL_Texture* sTexture;

	int sWidth;
	int sHeight;
	int squirrel_xpos;
	int squirrel_ypos;
	int random_y;
};



Squirrel::Squirrel()
{

	sWidth = 0;
	sHeight = 0;
	sTexture = NULL;
	squirrel_xpos = rand() % 262 + 288; //between 288 and 550
	std::cout << squirrel_xpos << std::endl;
	squirrel_ypos = 0;
	random_y = rand() %200;
}

Squirrel::~Squirrel()
{

	free();
}

bool Squirrel::loadFromFile( std::string path )
{
	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );

	if (loadedSurface == NULL)
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;

	else
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);

		if (newTexture == NULL)
			std::cout << "Unable to create texture from " << path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		else
		{
			sWidth = loadedSurface->w;
			sHeight = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}

	sTexture = newTexture;
	return sTexture != NULL;
}

void Squirrel::free()
{
	if (sTexture != NULL)
	{
		SDL_DestroyTexture(sTexture);
		sTexture = NULL;
		sWidth = 0;
		sHeight = 0;
	}
}

void Squirrel::render(int x, int y, SDL_Rect* clip)
{
	SDL_Rect renderQuad = { x, y, sWidth, sHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, sTexture, clip, &renderQuad ); 
}

int Squirrel::getWidth()
{
	return sWidth;
}

int Squirrel::getHeight()
{
	return sHeight;
}

int Squirrel::getXPos()
{
	return squirrel_xpos;
}

int Squirrel::getRandom(){
	return random_y;
}

int Squirrel::getYPos()
{
	return squirrel_ypos;
}

// void Squirrel::setXPos()
// {
// 	squirrel_xpos =  SCREEN_WIDTH / 2 ; // const
// }

void Squirrel::setYPos(int y)
{
	squirrel_ypos = ( getHeight() / 10 ) + y- random_y;
}


