//Using SDL and standard IO
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>
#include <cmath>
#include <sstream>

#include "vectorsquirrel.cpp"

#include "vectoracorn.cpp"

#include "scoreboard.cpp"

//Screen dimension constants
const int SCREEN_WIDTH = 770;
const int SCREEN_HEIGHT = 770;
int score=0;

class Sprite_Motion // class for texture
{
	public:
		
		Sprite_Motion(); // default constructor

		~Sprite_Motion(); // deconstructor

		bool loadFromFile( std::string path ); // imports the image

		void free(); // frees the memory/ deallocator

		// void setColor( Uint8 red, Uint8 green, Uint8 blue ); // for color

		//from the SDL_image library:

		// void setBlendMode( SDL_BlendMode blending ); // for blending

		// void setAlpha( Uint8 alpha ); // alpha modulation
		
		void render( int x, int y, SDL_Rect* clip = NULL ); // rendering @ specified point

		int getWidth(); // returns image width
		int getHeight(); // returns image height

	private:
		SDL_Texture* mTexture; // texture

		int mWidth; // image width
		int mHeight; // image height
		// bool isMoving;
};

// GLOBAL VARIABLES FOR PROGRAM

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

void checkCollision(SDL_Rect* squirrel, SDL_Rect* leprechaun, int squirrel_x, int squirrel_y, int leprechaun_x, int leprechaun_y, int k, int i);

int KILL = 0;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;
	
//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;

//The image we will load and show on the screen
SDL_Surface* gXOut = NULL;

SDL_Renderer* gRenderer = NULL; // window renderer

const int WALKING_ANIMATION_FRAMES = 4; // animation
SDL_Rect gSpriteClips[ WALKING_ANIMATION_FRAMES ];
Sprite_Motion gSpriteSheetTexture;

void checkLepCollision(SDL_Rect* squirrel, SDL_Rect* leprechaun, int squirrel_x, int squirrel_y, int leprechaun_x, int leprechaun_y, int k);

bool quit = false;

int levels[3]={6,12,30};
int levelcounter = 0;

// JUST ADDED THESE IN:
const int SQUIRREL_ANIMATION_FRAMES = 4; // animation
SDL_Rect gSquirrelClips[ SQUIRREL_ANIMATION_FRAMES ];
std::vector<Squirrel> gSquirrelSheetTexture; // instanciation of class


std::vector<Acorn> gAcornTexture; // instanciation of class for acorn
const int ACORN_FRAME = 1; // animation
SDL_Rect gAcornClip[ ACORN_FRAME ];

Scoreboard gTitleTexture;
Scoreboard gScoreTexture;

TTF_Font *gFont = NULL;


// from the SDL_image library : ...

// void Sprite_Motion::setColor( Uint8 red, Uint8 green, Uint8 blue )
// {
// 	// SDL_SetTextureColorMod( mTexture, red, green, blue );
// }

// void Sprite_Motion::setBlendMode( SDL_BlendMode blending )
// {
// 	// SDL_SetTextureBlendMode( mTexture, blending );
// }
		
// void Sprite_Motion::setAlpha( Uint8 alpha )
// {
// 	// SDL_SetTextureAlphaMod( mTexture, alpha );
// }

void Sprite_Motion::render( int x, int y, SDL_Rect* clip )
{
	SDL_Rect renderQuad = { x, y, mWidth, mHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, mTexture, clip, &renderQuad ); //renders to screen
}

int Sprite_Motion::getWidth() // returns image width
{
	return mWidth;
}

int Sprite_Motion::getHeight() // returns image height
{
	return mHeight;
}

Sprite_Motion::Sprite_Motion() // initialize
{
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

Sprite_Motion::~Sprite_Motion() // frees memory
{
	free();
}

void Sprite_Motion::free()
{
	//Free texture if it exists
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

bool Sprite_Motion::loadFromFile( std::string path )
{
	free(); // clears any texture that is already there

	SDL_Texture* newTexture = NULL; // texture

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() ); //loading image
	
	if( loadedSurface == NULL )
	{
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;
	}
	else
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			std::cout<< "Unable to create texture from " <<  path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		}
		else
		{
			mWidth = loadedSurface->w; // image width
			mHeight = loadedSurface->h; // image height
		}

		SDL_FreeSurface( loadedSurface ); // clears the surface for new clip
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}


bool init()
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			std::cout << "Warning: Linear texture filtering not enabled!";
		}
		//Create window
		gWindow = SDL_CreateWindow( "VIDEO GAME!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			//Get window surface
			gScreenSurface = SDL_GetWindowSurface( gWindow );
// ****************************************************************** // 
			// gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_SOFTWARE | SDL_RENDERER_ACCELERATED );
			// gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED );  // makes it seg fault
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED || SDL_RENDERER_PRESENTVSYNC ); // seg faults
			if( gRenderer == NULL )
			{
				std::cout << "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					std::cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
					success = false;
				}
				//Initialize SDL_ttf
                if( TTF_Init() == -1 )
                {
                    std::cout << "SDL_ttf could not initialize! SDL_ttf Error: " << TTF_GetError() << std::endl;
                    success = false;
                }

			}
		}
	}

	return success;
}

bool loadMedia()
{
	//Loading success flag
	bool success = true;

	//Load splash image
	gXOut = SDL_LoadBMP( "road2.bmp" );
	if( gXOut == NULL )
	{
		std::cout << "Unable to load image " << "road2.bmp" << "! SDL Error: " << SDL_GetError() << std::endl;
		success = false;
	}

	if( !gSpriteSheetTexture.loadFromFile( "leprechaun.png" ) ) // sprite sheet
	{
		std::cout << "Failed to load walking animation texture!" << std::endl; // failed load
		success = false;
	}
	else
	{
		// image seperated into sprite sheets
		gSpriteClips[ 0 ].x =   0;
		gSpriteClips[ 0 ].y = 150;
		gSpriteClips[ 0 ].w = 30;
		gSpriteClips[ 0 ].h = 46;

		gSpriteClips[ 1 ].x = 32;
		gSpriteClips[ 1 ].y = 150;
		gSpriteClips[ 1 ].w = 30;
		gSpriteClips[ 1 ].h = 46;
		
		gSpriteClips[ 2 ].x = 64;
		gSpriteClips[ 2 ].y = 150;
		gSpriteClips[ 2 ].w = 30;
		gSpriteClips[ 2 ].h = 46;

		gSpriteClips[ 3 ].x = 96;
		gSpriteClips[ 3 ].y = 150;
		gSpriteClips[ 3 ].w = 30;
		gSpriteClips[ 3 ].h = 46;
	}
	/*****************************************************/
	// JUST ADDED THIS
	for (int h = 0; h < gSquirrelSheetTexture.size();h++){

		if( !gSquirrelSheetTexture[h].loadFromFile( "squirrel.png", gRenderer ) ) // sprite sheet
		{
			std::cout << "Failed to load walking animation texture!" << std::endl; // failed load
			success = false;
		}
		else
		{
			// image seperated into sprite sheets
			gSquirrelClips[ 0 ].x =  0;
			gSquirrelClips[ 0 ].y =  0;
			gSquirrelClips[ 0 ].w = 34;
			gSquirrelClips[ 0 ].h = 41;

			gSquirrelClips[ 1 ].x = 34;
			gSquirrelClips[ 1 ].y =  0;
			gSquirrelClips[ 1 ].w = 34;
			gSquirrelClips[ 1 ].h = 41;
			
			gSquirrelClips[ 2 ].x = 68;
			gSquirrelClips[ 2 ].y = 0;
			gSquirrelClips[ 2 ].w = 34;
			gSquirrelClips[ 2 ].h = 41;

			gSquirrelClips[ 3 ].x = 68;
			gSquirrelClips[ 3 ].y = 0;
			gSquirrelClips[ 3 ].w = 34;
			gSquirrelClips[ 3 ].h = 41;

		}
	}

	/*****************************************************/
	// JUST ADDED THIS 2
	for (int h = 0; h < gAcornTexture.size();h++){

		if( !gAcornTexture[h].loadFromFile( "acorn3.png", gRenderer ) ) // sprite sheet
		{
			std::cout << "Failed to load acorn!" << std::endl; // failed load
			success = false;
		}
		else
		{
			// image seperated into sprite sheets
			gAcornClip[ 0 ].x = 111;
			gAcornClip[ 0 ].y = 64;
			gAcornClip[ 0 ].w = 18;
			gAcornClip[ 0 ].h = 18;

		}
	}
	// ********************************************************************
	// added for font
	gFont = TTF_OpenFont( "Caviar_Dreams_Bold.ttf", 22 );
	if( gFont == NULL )
	{
		std::cout << "Failed to load lazy font! SDL_ttf Error: " << TTF_GetError() << std::endl;
		success = false;
	}
	else
	{
		//Render text
		SDL_Color textColor = { 0, 0, 0 };
		if( !gTitleTexture.loadFromRenderedText( "Shoot Like a Champion", textColor , gFont, gRenderer) )
		{
			std::cout << "Failed to render text texture!" << std::endl;
			success = false;
		}
		std::stringstream s;
		std::string points = "Score: ";
		s << points << score;
		std::string result = s.str();
		if( !gScoreTexture.loadFromRenderedText( result, textColor , gFont, gRenderer) )
		{
			std::cout << "Failed to render text texture!" << std::endl;
			success = false;
		}
		// gFont = TTF_OpenFont( "3Dumb-webfont.ttf", 36 );
		// if( !gStartTexture.loadFromRenderedText( "Start", textColor ) )
		// {
		// 	printf( "Failed to render text texture!\n" );
		// 	success = false;
		// }
		// if( !gInstructionTexture.loadFromRenderedText( "Instructions", textColor ) )
		// {
		// 	printf( "Failed to render text texture!\n" );
		// 	success = false;
		// }
		// if( !gOptionsTexture.loadFromRenderedText( "Options", textColor ) )
		// {
		// 	printf( "Failed to render text texture!\n" );
		// 	success = false;
		// }
		// if( !gQuitTexture.loadFromRenderedText( "Quit", textColor ) )
		// {
		// 	printf( "Failed to render text texture!\n" );
		// 	success = false;
		// }
	}



	return success;
}

void close()
{
	//Deallocate surface
	SDL_FreeSurface( gXOut );
	gXOut = NULL;

	//JUST ADDED THIS:
	for (int i = 0; i < gSquirrelSheetTexture.size();i++){
		gSquirrelSheetTexture[i].free();
	}

	for (int i = 0; i < gAcornTexture.size();i++){
		gAcornTexture[i].free();
	}

	gSpriteSheetTexture.free();

	// for font stuff
	gTitleTexture.free();
	TTF_CloseFont( gFont );
	gFont = NULL;

	SDL_DestroyRenderer( gRenderer );

	//Destroy window
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	TTF_Quit(); // for font

	IMG_Quit();

	//Quit SDL subsystems
	SDL_Quit();
}

void checkCollision(SDL_Rect* squirrel, SDL_Rect* acorn, int squirrel_x, int squirrel_y, int acorn_x, int acorn_y, int k, int i)
{
	int leftS, leftL;
	int rightS, rightL;
	int topS, topL;
	int botS, botL;

	//bool isAlive;
	// sides of squirrel
	leftS = squirrel_x;
	rightS = squirrel_x + squirrel->w;
	topS = squirrel_y;
	botS = squirrel_y + squirrel->h;

	// sides of leprechaun
	leftL = acorn_x;
	rightL = acorn_x + acorn->w;
	topL = acorn_y;
	botL = acorn_y + acorn->h;

	//If any of the sides from A are outside of B
    if( botS <= topL )
    {
      //  gSquirrelSheetTexture[k].setIsAlive(true);
        return;
    }

    if( topS >= botL )
    {
      //  gSquirrelSheetTexture[k].setIsAlive(true);
        return;
    }

    if( rightS <= leftL )
    {
     //   gSquirrelSheetTexture[k].setIsAlive(true);
        return;
    }

    if( leftS >= rightL )
    {
    //	gSquirrelSheetTexture[k].setIsAlive(true);
        return;
    }

    //If none of the sides from squirrel are outside acorn
    gSquirrelSheetTexture[k].setIsAlive(false);
    gSquirrelSheetTexture[k].setYPos(950);
    gSquirrelSheetTexture[k].setXPos(950);
    gAcornTexture[i].setIsAlive(false);
    gAcornTexture[i].setYPos(950);
    gAcornTexture[i].setXPos(950);
    score +=20;
   // gSquirrelSheetTexture[k].setRandom(900);
    return;	
}

void checkLepCollision(SDL_Rect* squirrel, SDL_Rect* leprechaun, int squirrel_x, int squirrel_y, int leprechaun_x, int leprechaun_y, int k)
{
	int leftS, leftL;
	int rightS, rightL;
	int topS, topL;
	int botS, botL;

	//bool isAlive;
	// sides of squirrel
	if (levels[levelcounter] == 6){
		leftS = squirrel_x;
		rightS = squirrel_x + squirrel->w/1.6;
		topS = squirrel_y;
		botS = squirrel_y + squirrel->h/1.6;

		// sides of leprechaun
		leftL = leprechaun_x;
		rightL = leprechaun_x + leprechaun->w/1.3;
		topL = leprechaun_y;
		botL = leprechaun_y + leprechaun->h/1.3;
	}

	else {
		leftS = squirrel_x;
		rightS = squirrel_x + squirrel->w/1.9;
		topS = squirrel_y;
		botS = squirrel_y + squirrel->h/1.8;

		// sides of leprechaun
		leftL = leprechaun_x;
		rightL = leprechaun_x + leprechaun->w/1.6;
		topL = leprechaun_y;
		botL = leprechaun_y + leprechaun->h/1.2;
	}	
	
	// if (levels[2] == 30){
	// 	leftS = squirrel_x;
	// 	rightS = squirrel_x + squirrel->w/1.9;
	// 	topS = squirrel_y;
	// 	botS = squirrel_y + squirrel->h/1.8;

	// 	// sides of leprechaun
	// 	leftL = leprechaun_x;
	// 	rightL = leprechaun_x + leprechaun->w/1.6;
	// 	topL = leprechaun_y;
	// 	botL = leprechaun_y + leprechaun->h/1.2;
	// }	
	
	// std::cout << "Squirrel: " << leftS << " " << rightS << " " << topS << " " << botS << std::endl;
	// std::cout << "Lep: " << leftL << " " << rightL << " " << topL << " " << botL << std::endl;
	//If any of the sides from A are outside of B
    if( botS <= topL )
    {
      //  gSquirrelSheetTexture[k].setIsAlive(true);
        return;
    }

    if( topS >= botL )
    {
      //  gSquirrelSheetTexture[k].setIsAlive(true);
        return;
    }

    if( rightS <= leftL )
    {
     //   gSquirrelSheetTexture[k].setIsAlive(true);
        return;
    }

    if( leftS >= rightL )
    {
    //	gSquirrelSheetTexture[k].setIsAlive(true);
        return;
    }

    //If none of the sides from squirrel are outside leprechaun
    std::cout << botS << " " << topL << std::endl;
    quit = true;
    std::cout << "YOU LOST! LOSER." << std::endl;

   // gSquirrelSheetTexture[k].setRandom(900);
    return;	
}


int main( int argc, char* args[] )
{
	int x = 0; // change in the x
	int y = 0; // change in the y
	int xposition, yposition;
	bool isMoving = false;
	int AcornAlive = 0;
	int enemies_total = 6;
	std::stringstream s;
	std::string points = "Score: ";

	//std::cout << levels[0] << std::endl;

	// JUST ADDED THIS:
	srand(time(0));
	int y_squir = 0; // change in the y

	for (int j = 0; j<levels[levelcounter];j++){
		Squirrel temp;
		gSquirrelSheetTexture.push_back(temp);
	}

	for (int j = 0; j<levels[levelcounter]*2;j++){
		Acorn temp;
		gAcornTexture.push_back(temp);
	}



	//Start up SDL and create window
	if( !init() )
	{
		std::cout << "Failed to initialize!" << std::endl;
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			std::cout << "Failed to load media!" << std::endl;
		}
		else
		{			
			//Main loop flag

			//Event handler
			SDL_Event e;

			int frame = 0; // starts at the first clip
			int frame_squir = 0;

			//While application is running
			while( !quit )
			{
				SDL_Delay(1);		
				//Handle events on queue
				SDL_PollEvent( &e );

				switch(e.type)
				{
					case SDL_QUIT:
						quit = true;
						break;
					case SDL_KEYDOWN:
						switch(e.key.keysym.sym)
						{
							case SDLK_LEFT:  x--; isMoving = true; break;
			                case SDLK_RIGHT: x++; isMoving = true; break;
			                case SDLK_UP:    y--; isMoving = true; break;
			                case SDLK_SPACE: AcornAlive++; break;
						}
						break;
					case SDL_KEYUP:
						switch(e.key.keysym.sym)
						{
							case SDLK_LEFT:  isMoving = false; break;
			                case SDLK_RIGHT: isMoving = false; break;
			                case SDLK_UP:    isMoving = false; break;
						}
						break;
				}

				//Apply the image
				SDL_BlitSurface( gXOut, NULL, gScreenSurface, NULL );
			
				//Update the surface
				// SDL_UpdateWindowSurface( gWindow );


				// SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF ); // clears screen
				// SDL_RenderClear( gRenderer );
// frame / 4
				SDL_Rect* currentClip = &gSpriteClips[ frame / 4 ];
				xposition = (( SCREEN_WIDTH - currentClip->w ) / 2) + x;
				yposition = (9.75 * ( SCREEN_HEIGHT - currentClip->h ) / 10) + y;

				SDL_Rect* currentClipSquir = &gSquirrelClips[ frame_squir / 4 ];
				++y_squir;

				// added this!!!!!!!!!!!!!!!!!!!!1
				SDL_Rect* currentClipAcorn = & gAcornClip[ 0 ];


				if (xposition >= 550)
				{
					xposition = 550;
					x = 180;
				}
				if (xposition <= 285) 
				{
					xposition = 285;
					x = -81;
				}

				if (yposition <= 7) 
				{
					bool check = true;
					for (int w=0; w <gSquirrelSheetTexture.size();w++){
						if (gSquirrelSheetTexture[w].getIsAlive()){
							check = false;
							std::cout <<"you lose" << std::endl;
							quit = true;
						}
					}
					if (check){
						std::cout << "you win!" << std::endl;
						quit = true;
					}
					yposition = 7;
				}


				for (int k=0; k<gSquirrelSheetTexture.size();k++){
					gSquirrelSheetTexture[k].setYPos(y_squir);
				}
				

				for ( int k=0; k< gSquirrelSheetTexture.size(); k++){
					// int sub = gSquirrelSheetTexture[k].getRandom(); squirrel_x, squirrel_y, leprechaun_x, leprechaun_y
					if ( (gSquirrelSheetTexture[k].getYPos() >= 730)  )
					{
						// y = 0;
						gSquirrelSheetTexture[k].setYPos(950);

					}

					for (int i =0; i <gAcornTexture.size(); i++){
						if (gAcornTexture[i].getIsAlive()) //FIXEDDDDDDDDDDDDDDD ITTTTTTTTTTTTTTTTTTTTT
						{
							if (gSquirrelSheetTexture[k].getIsAlive()){
								checkCollision(currentClipSquir, currentClipAcorn, gSquirrelSheetTexture[k].getXPos() , gSquirrelSheetTexture[k].getYPos() , gAcornTexture[i].getXPos() , gAcornTexture[i].getYPos(), k, i);
							}
						}
						// if (gSquirrelSheetTexture[k].getIsAlive()){
						// 	checkCollision(currentClipSquir, currentClipAcorn, gSquirrelSheetTexture[k].getXPos() , gSquirrelSheetTexture[k].getYPos() , gAcornTexture[i].getXPos() , gAcornTexture[i].getYPos(), k, i);
						// }
					}
				}

				if (AcornAlive)
				{// for ( int k=0; k< gAcornTexture.size(); k++){
					for ( int k=0; k< AcornAlive; k++){ 
						if (k<gAcornTexture.size()){
							gAcornTexture[k].setMove(5);
							gAcornTexture[k].setXPos(xposition);
							gAcornTexture[k].setYPos(yposition);
							if (gAcornTexture[k].getIsAlive())
								gAcornTexture[k].render(gAcornTexture[k].getXPos() , gAcornTexture[k].getYPos() , currentClipAcorn, gRenderer);
						}
					}
				}

				// for ( int k=0; k< gAcornTexture.size(); k++){
				// 	gAcornTexture[k].render(gAcornTexture[k].getXPos() , gAcornTexture[k].getYPos() , currentClipAcorn, gRenderer);
				// }

				if ( isMoving )
				{
					gSpriteSheetTexture.render( xposition, yposition , currentClip );

				}
				else
				{
					frame = 1;
					SDL_Rect* currentClip = &gSpriteClips[ frame / 4 ];

					gSpriteSheetTexture.render( xposition , yposition , currentClip );
				}

				for ( int k=0; k<gSquirrelSheetTexture.size(); k++){
					checkLepCollision(currentClipSquir, currentClip, gSquirrelSheetTexture[k].getXPos() , gSquirrelSheetTexture[k].getYPos() , xposition , yposition , k);
					if ( gSquirrelSheetTexture[k].getIsAlive() )
						gSquirrelSheetTexture[k].render( gSquirrelSheetTexture[k].getXPos() , gSquirrelSheetTexture[k].getYPos() , currentClipSquir , gRenderer);
					// std::cout << k << std::endl;
					// std::cout << gSquirrelSheetTexture[k].getYPos() << std::endl;
					//gSquirrelSheetTexture[k].render( gSquirrelSheetTexture[k].getXPos() +20, gSquirrelSheetTexture[k].getYPos() +50, currentClip );
				}
				
				std::stringstream s;
				std::string points = "Score: ";
				s << points << score;
				std::string result = s.str();
				gScoreTexture.free();
				gScoreTexture.loadFromRenderedText( result, {0,0,0} , gFont, gRenderer);

				// for font:
				gTitleTexture.render( 10 , 10 , gRenderer);
				gScoreTexture.render( 10 , 30 , gRenderer);

				SDL_RenderPresent( gRenderer );
				//score +=2;
				++frame; // moves onto next frame while there is no user input
				++frame_squir;
				if( frame / 4 >= WALKING_ANIMATION_FRAMES ) // starts clips all over again
				{
					frame = 0;
				}
				if( frame_squir / 4 >= SQUIRREL_ANIMATION_FRAMES ) // starts clips all over again
				{
					frame_squir = 0;
				}
				//bool check=true;
				// for (int d=0; d<gSquirrelSheetTexture.size();d++){
				// 	if (gSquirrelSheetTexture[d].getYPos() >= 875 && gSquirrelSheetTexture[d].getIsAlive()){
				// 		quit = true;
				// 	}
				// }
			}
		}
	}

	//Free resources and close SDL
	close();

	return 0;
}
