// Squirrel class
//Necessay Libraries
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>


class Squirrel
{
public:
	Squirrel(); // constructor
	~Squirrel(); //deconstructor

	bool loadFromFile(std::string path, SDL_Renderer* gRenderer); //loaading images
	void free(); //freeing resources

	void render( int x, int y, SDL_Rect* clip = NULL , SDL_Renderer* gRenderer = NULL); //rendering the squirrel at inputted location

	int getWidth(); //returns width of clip
	int getHeight(); //returns height of clip
	int getXPos(); //returns x positon of clip
	int getYPos(); //returns y position of clip
	int getRandom(); //random number generator
	void setYPos(int y); //sets the new y position
	void setXPos(int x); //sets new x position
	bool getIsAlive(); //returns life status of squirrel
	void setIsAlive(bool value); //sets the life status of squirrel

private:
	SDL_Texture* sTexture; //texture for the squirrel

	int sWidth; //width of clip
	int sHeight; //height of clip
	int squirrel_xpos; //x position
	int squirrel_ypos; //y position
	int random_y; //random number used to initialize squirrel position
	bool isAlive; //life status
};



Squirrel::Squirrel() // constructor
{
	// initialized values
	sWidth = 0;
	sHeight = 0;
	sTexture = NULL;
	squirrel_xpos = rand() % 262 + 288; //between 288 and 550 so its on the road
	squirrel_ypos = 0;
	random_y = rand() %900; // used to spread out the squirrel y position
	isAlive = true;
}

Squirrel::~Squirrel()
{
	//resources freed
	free();
}

bool Squirrel::loadFromFile( std::string path , SDL_Renderer* gRenderer)
{
	free(); // free (clear) what was before

	SDL_Texture* newTexture = NULL; //temp texture

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() ); //load in sprite sheet

	if (loadedSurface == NULL) //sprite sheet failed to initialize
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;

	else // sprite sheet initialized
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);

		if (newTexture == NULL) // texture loading failed
			std::cout << "Unable to create texture from " << path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		else // retriving width and heght
		{
			sWidth = loadedSurface->w;
			sHeight = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface); // free surface
	}

	sTexture = newTexture; 
	return sTexture != NULL; // returns successful loading
}

void Squirrel::free() // frees resources
{
	if (sTexture != NULL)
	{
		SDL_DestroyTexture(sTexture); // destroys texture
		sTexture = NULL;
		sWidth = 0;
		sHeight = 0;
	}
}

void Squirrel::render(int x, int y, SDL_Rect* clip, SDL_Renderer* gRenderer) // renders image
{
	SDL_Rect renderQuad = { x, y, sWidth, sHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, sTexture, clip, &renderQuad ); 
}

int Squirrel::getWidth() //width
{
	return sWidth;
}

int Squirrel::getHeight() //height
{
	return sHeight;
}

int Squirrel::getXPos() //x position
{
	return squirrel_xpos;
}

int Squirrel::getRandom() // random number
{ 
	return random_y;
}

int Squirrel::getYPos() //y position
{
	return squirrel_ypos;
}

void Squirrel::setXPos(int x) //sets x
{
 	squirrel_xpos =  x; // const
}

void Squirrel::setYPos(int y) //sets y
{
	squirrel_ypos = ( getHeight() / 10 ) + y- random_y;
}

bool Squirrel::getIsAlive() //passes life status
{
	return isAlive;
}

void Squirrel::setIsAlive(bool value) //sets life status
{
	isAlive = value;
}
