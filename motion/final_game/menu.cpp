//Menu Class

//Using SDL, SDL_image, SDL_ttf, standard IO, math, and strings

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <cmath>

class MainMenu
{
	public:
		MainMenu(); //Initializes variables

		~MainMenu(); //Deallocates memory
				
		bool loadFromRenderedText_menu( std::string textureText, SDL_Color textColor, TTF_Font *gFont_menu , SDL_Renderer* gRenderer); //Creates image from font string
		
		void free_menu(); //Deallocates texture

		void render_menu( int x, int y, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE , SDL_Renderer* gRenderer=NULL); //Renders texture at given point
		
		int getWidth_menu(); //Gets image dimensions
		int getHeight_menu();

	private:
		SDL_Texture* menuTexture; //The actual hardware texture

		int mWidth; //Image dimensions
		int mHeight;

};

MainMenu::MainMenu() //constructor
{
	//Initialized values
	menuTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

MainMenu::~MainMenu() //deconstructor
{
	free_menu(); //Deallocate
}

bool MainMenu::loadFromRenderedText_menu( std::string textureText, SDL_Color textColor , TTF_Font *gFont_menu, SDL_Renderer* gRenderer) //tries to load image
{
	
	free_menu(); //Get rid of preexisting texture

	
	SDL_Surface* textSurface = TTF_RenderText_Solid( gFont_menu, textureText.c_str(), textColor ); //render_menu text surface
	if( textSurface == NULL ) //not loaded
	{
		printf( "Unable to render_menu text surface! SDL_ttf Error: %s\n", TTF_GetError() );
	}
	else
	{
        menuTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface ); //Create texture from surface pixels
		if( menuTexture == NULL )
		{
			printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
		}
		else
		{
			mWidth = textSurface->w; //Get image dimensions
			mHeight = textSurface->h;
		}

		SDL_FreeSurface( textSurface ); //Get rid of old surface
	}
	
	return menuTexture != NULL; //Return success
}

void MainMenu::free_menu() //deallocates
{
	if( menuTexture != NULL ) //free_menu texture if it exists
	{
		SDL_DestroyTexture( menuTexture );
		menuTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

void MainMenu::render_menu( int x, int y, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip , SDL_Renderer* gRenderer)
{
	SDL_Rect renderQuad = { x, y, mWidth, mHeight }; //Set rendering space and render_menu to screen

	if( clip != NULL ) //Set clip rendering dimensions
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopyEx( gRenderer, menuTexture, clip, &renderQuad, angle, center, flip ); //render_menu to screen
}

int MainMenu::getWidth_menu() //returns width
{
	return mWidth;
}

int MainMenu::getHeight_menu() //returns height
{
	return mHeight;
}