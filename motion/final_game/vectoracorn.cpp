// Acorn class
//Necessary Libraries
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>

class Acorn
{
public:
	Acorn(); //constructor
	~Acorn(); //deconstructor

	bool loadFromFile(std::string path, SDL_Renderer* gRenderer); //determines if image loaded successfully
	void free(); // frees resources

	void render( int x, int y, SDL_Rect* clip = NULL , SDL_Renderer* gRenderer = NULL); // renders image to window

	int getWidth(); //returns width
	int getHeight(); //returns height
	int getXPos(); //return x position
	int getYPos(); //returns y position
	int getRandom(); //returns random number 
	int getMovement(); //retirves how far its moved
	bool getEncounter(); // returns if encountered before
	void setEncounter(bool); //checks to see if squirrel has been encountered before
	void setMove(int); // sets how far its moved
	void setXPos(int x); //sets x
	void setYPos(int y); //sets y
	bool getIsAlive(); //returns alive status
	void setIsAlive(bool value); //changes alive status


private:
	SDL_Texture* aTexture; //texture to render

	int aWidth; //width
	int aHeight; //height
	int acorn_xpos; //x position
	int acorn_ypos; //y position
	int random_y; //random number generator
	bool acorn_isAlive; // life status
	bool encountered; //determines if encountered before
	int movement; //how far its moved
};



Acorn::Acorn() //consturctor
{
	//initialized values
	aWidth = 0;
	aHeight = 0;
	aTexture = NULL;
	encountered = false;
	acorn_xpos = -50; //initilaized to off screen view
	acorn_ypos = -50;
	random_y = rand() %200; //random number generator
	acorn_isAlive = true;
	movement = 5;
}

Acorn::~Acorn() // deconstructor
{
	free();
}

bool Acorn::loadFromFile( std::string path , SDL_Renderer* gRenderer) //checks to see if image was loaded correctly
{
	free();

	SDL_Texture* newTexture = NULL; //temo tecture

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() ); //loads image

	if (loadedSurface == NULL) //image not loaded
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;

	else //image loaded
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);

		if (newTexture == NULL)
			std::cout << "Unable to create texture from " << path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		else //retrives width and height
		{
			aWidth = loadedSurface->w;
			aHeight = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}

	aTexture = newTexture;
	return aTexture != NULL;
}

void Acorn::free() //free resources
{
	if (aTexture != NULL)
	{
		SDL_DestroyTexture(aTexture); //destroys texture
		aTexture = NULL;
		aWidth = 0;
		aHeight = 0;
	}
}

void Acorn::render(int x, int y, SDL_Rect* clip, SDL_Renderer* gRenderer) //renders image
{
	SDL_Rect renderQuad = { x, y, aWidth, aHeight }; // rendering

 	if( clip != NULL ) // deternmines width/height of image loaded
 	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, aTexture, clip, &renderQuad ); //draws image
}

int Acorn::getWidth() //returns width
{
	return aWidth;
}

int Acorn::getHeight() //returns height
{
	return aHeight;
}

int Acorn::getXPos() //returns x position
{
	return acorn_xpos;
}

int Acorn::getRandom() //returns random generated
{
	return random_y;
}

bool Acorn::getEncounter() //returns if encountered 
{
	return encountered;
}

void Acorn::setEncounter(bool value) //value to determine if squirrel has been encountered before
{
	encountered = value;
}

int Acorn::getYPos() //returns y position
{
	return acorn_ypos;
}

void Acorn::setXPos(int x) //returns x position
{
	acorn_xpos = x;
}

void Acorn::setYPos(int y) //sets new y position
{
	acorn_ypos = y  - getMovement();
}

bool Acorn::getIsAlive() //returns status
{
	return acorn_isAlive;
}

void Acorn::setIsAlive(bool value) //sets life status
{
	acorn_isAlive = value;
}

int Acorn::getMovement() //retrieves how far its moved so far
{
	return movement;
}

void Acorn::setMove(int y) // increments how far its moved
{
	movement = movement + y;
}
