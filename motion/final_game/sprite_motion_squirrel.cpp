//Using SDL and standard IO
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>
#include <cmath>
#include <sstream>

#include "vectorsquirrel.cpp"
#include "vectoracorn.cpp"
#include "menu.cpp"
#include "scoreboard.cpp"

//Screen dimension constants
const int SCREEN_WIDTH = 770;
const int SCREEN_HEIGHT = 770;
int score=0;

//The window we'll be rendering to
SDL_Window* gWindow = NULL;
	
//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;

//The image we will load and show on the screen
SDL_Surface* gXOut = NULL;

SDL_Renderer* gRenderer = NULL; // window renderer

const int WALKING_ANIMATION_FRAMES = 4; // animation frames for leprechaun
SDL_Rect gSpriteClips[ WALKING_ANIMATION_FRAMES ]; //determines which leprechaun clip to use

TTF_Font *gFont_menu = NULL; // font for menu

//Rendered texture for Menu items
MainMenu gTitleTexture_menu;
MainMenu LevelOneTexture;
MainMenu LevelTwoTexture;
MainMenu LevelThreeTexture;
MainMenu gQuitTexture;

bool init_menu()
{
	bool success = true; //Initialization flag

	if( SDL_Init( SDL_INIT_VIDEO ) < 0 ) //Initialize SDL
	{
		printf( "SDL could not initialize! SDL Error: %s\n", SDL_GetError() );
		success = false;
	}
	else
	{
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) ) //Set texture filtering to linear
		{
			printf( "Warning: Linear texture filtering not enabled!" );
		}

		//Create window
		gWindow = SDL_CreateWindow( "VIDEO GAME!!!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );

		if( gWindow == NULL )
		{
			printf( "Window could not be created! SDL Error: %s\n", SDL_GetError() );
			success = false;
		}

		else
		{
			//Create vsynced renderer for window
			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );
			
			if( gRenderer == NULL )
			{
				printf( "Renderer could not be created! SDL Error: %s\n", SDL_GetError() );
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF ); //Initialize renderer color
 
				int imgFlags = IMG_INIT_PNG; //Initialize PNG loading
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					printf( "SDL_image could not initialize! SDL_image Error: %s\n", IMG_GetError() );
					success = false;
				}

				if( TTF_Init() == -1 ) //Initialize SDL_ttf
				{
					printf( "SDL_ttf could not initialize! SDL_ttf Error: %s\n", TTF_GetError() );
					success = false;
				}
			}
		}
	}

	return success;
}

bool loadMedia_menu() // loads necessary fonts for menu text rendering
{
	bool success = true; //Loading success flag

	gFont_menu = TTF_OpenFont( "3Dumb-webfont.ttf", 52 ); //Open the font

	if( gFont_menu == NULL ) //if it was unsuccessful
	{
		printf( "Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError() );
		success = false;
	}
	else
	{
		//render menu text
		SDL_Color textColor = { 0, 0, 0 };
		if( !gTitleTexture_menu.loadFromRenderedText_menu( "Shoot Like a Champion", textColor, gFont_menu, gRenderer) )
		{
			printf( "Failed to render_menu text texture!\n" );
			success = false;
		}

		gFont_menu = TTF_OpenFont( "3Dumb-webfont.ttf", 36 ); // changes size of the new text

		if( !LevelOneTexture.loadFromRenderedText_menu( "Level One", textColor, gFont_menu, gRenderer ) )
		{
			printf( "Failed to render_menu text texture!\n" );
			success = false;
		}
		if( !LevelTwoTexture.loadFromRenderedText_menu( "Level Two", textColor, gFont_menu, gRenderer ) )
		{
			printf( "Failed to render_menu text texture!\n" );
			success = false;
		}
		if( !LevelThreeTexture.loadFromRenderedText_menu( "Level Three", textColor, gFont_menu, gRenderer ) )
		{
			printf( "Failed to render_menu text texture!\n" );
			success = false;
		}
		if( !gQuitTexture.loadFromRenderedText_menu( "Quit", textColor, gFont_menu, gRenderer ) )
		{
			printf( "Failed to render_menu text texture!\n" );
			success = false;
		}
	}

	return success; //successful loading
}

void close_menu() //frees memory for menu items
{
	//free_menu loaded images
	gTitleTexture_menu.free_menu();
	LevelOneTexture.free_menu();
	LevelTwoTexture.free_menu();
	LevelThreeTexture.free_menu();

	gQuitTexture.free_menu();

	//free_menu global font
	TTF_CloseFont( gFont_menu );
	gFont_menu = NULL;

	//Destroy window	
	SDL_DestroyRenderer( gRenderer );
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	//quit_menu SDL subsystems
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
}

// Leprechaun (player) class
class Sprite_Motion // class for texture
{
	public:
		Sprite_Motion(); // default constructor

		~Sprite_Motion(); // deconstructor

		bool loadFromFile( std::string path ); // imports the image

		void free(); // frees the memory/ deallocator

		void render( int x, int y, SDL_Rect* clip = NULL ); // rendering @ specified point

		int getWidth(); // returns image width
		int getHeight(); // returns image height

	private:
		SDL_Texture* mTexture; // texture

		int mWidth; // image width
		int mHeight; // image height
};

// GLOBAL VARIABLES FOR PROGRAM/GAME

bool init(); //Starts up SDL and creates window

bool loadMedia(); //Loads media

void close(); //Frees media and shuts down SDL

void checkCollision(SDL_Rect* squirrel, SDL_Rect* leprechaun, int squirrel_x, int squirrel_y, int leprechaun_x, int leprechaun_y, int k, int i); //collision detection

int nutsLeft; // nut counter

void checkLepCollision(SDL_Rect* squirrel, SDL_Rect* leprechaun, int squirrel_x, int squirrel_y, int leprechaun_x, int leprechaun_y, int k); //collision detection 

bool quit = false; //quits game

int currentLevel; //level number

// Squirrel contant ints
const int SQUIRREL_ANIMATION_FRAMES = 4; // squirrel clips for animation
SDL_Rect gSquirrelClips[ SQUIRREL_ANIMATION_FRAMES ]; //determines which clip to render
std::vector<Squirrel> gSquirrelSheetTexture; // vector instanciation of class

Sprite_Motion gSpriteSheetTexture; //instanciation of leprechaun class

std::vector<Acorn> gAcornTexture; // vector instanciation of class for acorn
const int ACORN_FRAME = 1; // acorn clip number
SDL_Rect gAcornClip[ ACORN_FRAME ]; //renders clip to render

// Scoreboard objects rendered on window
Scoreboard gTitleTexture;
Scoreboard gScoreTexture;
Scoreboard gLevelTexture;
Scoreboard gNutTexture;

TTF_Font *gFont = NULL; //font for text on scoreboard

void Sprite_Motion::render( int x, int y, SDL_Rect* clip ) //used to determine what to render
{
	SDL_Rect renderQuad = { x, y, mWidth, mHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, mTexture, clip, &renderQuad ); //renders to screen
}

int Sprite_Motion::getWidth() // returns image width
{
	return mWidth;
}

int Sprite_Motion::getHeight() // returns image height
{
	return mHeight;
}

Sprite_Motion::Sprite_Motion() // initialize
{
	mTexture = NULL;
	mWidth = 0;
	mHeight = 0;
}

Sprite_Motion::~Sprite_Motion() // frees memory
{
	free();
}

void Sprite_Motion::free() //Free texture if it exists
{
	if( mTexture != NULL )
	{
		SDL_DestroyTexture( mTexture );
		mTexture = NULL;
		mWidth = 0;
		mHeight = 0;
	}
}

bool Sprite_Motion::loadFromFile( std::string path ) //atempts to load image used to animate leprechaun
{
	free(); // clears any texture that is already there

	SDL_Texture* newTexture = NULL; // texture

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() ); //loading image
	
	if( loadedSurface == NULL ) //image failed to load
	{
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;
	}
	else
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB( loadedSurface->format, 0, 0xFF, 0xFF ) );

        newTexture = SDL_CreateTextureFromSurface( gRenderer, loadedSurface );
		if( newTexture == NULL )
		{
			std::cout<< "Unable to create texture from " <<  path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		}
		else
		{
			mWidth = loadedSurface->w; // image width
			mHeight = loadedSurface->h; // image height
		}

		SDL_FreeSurface( loadedSurface ); // clears the surface for new clip
	}

	//Return success
	mTexture = newTexture;
	return mTexture != NULL;
}


bool init() //initializer for the game
{
	//Initialization flag
	bool success = true;

	//Initialize SDL
	if( SDL_Init( SDL_INIT_VIDEO ) < 0 )
	{
		std::cout << "SDL could not initialize! SDL_Error: " << SDL_GetError() << std::endl;
		success = false;
	}
	else
	{
		if( !SDL_SetHint( SDL_HINT_RENDER_SCALE_QUALITY, "1" ) )
		{
			std::cout << "Warning: Linear texture filtering not enabled!";
		}
		//Create window
		gWindow = SDL_CreateWindow( "VIDEO GAME!", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
		if( gWindow == NULL )
		{
			std::cout << "Window could not be created! SDL_Error: " << SDL_GetError() << std::endl;
			success = false;
		}
		else
		{
			//Get window surface
			gScreenSurface = SDL_GetWindowSurface( gWindow );

			gRenderer = SDL_CreateRenderer( gWindow, -1, SDL_RENDERER_ACCELERATED || SDL_RENDERER_PRESENTVSYNC ); // seg faults
			if( gRenderer == NULL )
			{
				std::cout << "Renderer could not be created! SDL Error: " << SDL_GetError() << std::endl;
				success = false;
			}
			else
			{
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );

				int imgFlags = IMG_INIT_PNG;
				if( !( IMG_Init( imgFlags ) & imgFlags ) )
				{
					std::cout << "SDL_image could not initialize! SDL_image Error: " << IMG_GetError() << std::endl;
					success = false;
				}
				//Initialize SDL_ttf
                if( TTF_Init() == -1 )
                {
                    std::cout << "SDL_ttf could not initialize! SDL_ttf Error: " << TTF_GetError() << std::endl;
                    success = false;
                }

			}
		}
	}

	return success;
}

bool loadMedia() //loads media stuff
{
	bool success = true; //Loading success flag

	gXOut = SDL_LoadBMP( "road2.bmp" ); //Load background image

	if( gXOut == NULL )
	{
		std::cout << "Unable to load image " << "road2.bmp" << "! SDL Error: " << SDL_GetError() << std::endl;
		success = false;
	}

	if( !gSpriteSheetTexture.loadFromFile( "leprechaun.png" ) ) // sprite sheet for leprechaun
	{
		std::cout << "Failed to load walking animation texture!" << std::endl; // failed load
		success = false;
	}
	else
	{
		// leprechaun image seperated into sprite sheets
		gSpriteClips[ 0 ].x =   0;
		gSpriteClips[ 0 ].y = 150;
		gSpriteClips[ 0 ].w = 30;
		gSpriteClips[ 0 ].h = 46;

		gSpriteClips[ 1 ].x = 32;
		gSpriteClips[ 1 ].y = 150;
		gSpriteClips[ 1 ].w = 30;
		gSpriteClips[ 1 ].h = 46;
		
		gSpriteClips[ 2 ].x = 64;
		gSpriteClips[ 2 ].y = 150;
		gSpriteClips[ 2 ].w = 30;
		gSpriteClips[ 2 ].h = 46;

		gSpriteClips[ 3 ].x = 96;
		gSpriteClips[ 3 ].y = 150;
		gSpriteClips[ 3 ].w = 30;
		gSpriteClips[ 3 ].h = 46;
	}

	// for squirrel

	for (int h = 0; h < gSquirrelSheetTexture.size();h++){

		if( !gSquirrelSheetTexture[h].loadFromFile( "squirrel.png", gRenderer ) ) // sprite sheet
		{
			std::cout << "Failed to load walking animation texture!" << std::endl; // failed load
			success = false;
		}
		else
		{
			// squirrel image seperated into sprite sheets
			gSquirrelClips[ 0 ].x =  0;
			gSquirrelClips[ 0 ].y =  0;
			gSquirrelClips[ 0 ].w = 34;
			gSquirrelClips[ 0 ].h = 41;

			gSquirrelClips[ 1 ].x = 34;
			gSquirrelClips[ 1 ].y =  0;
			gSquirrelClips[ 1 ].w = 34;
			gSquirrelClips[ 1 ].h = 41;
			
			gSquirrelClips[ 2 ].x = 68;
			gSquirrelClips[ 2 ].y = 0;
			gSquirrelClips[ 2 ].w = 34;
			gSquirrelClips[ 2 ].h = 41;

			gSquirrelClips[ 3 ].x = 68;
			gSquirrelClips[ 3 ].y = 0;
			gSquirrelClips[ 3 ].w = 34;
			gSquirrelClips[ 3 ].h = 41;

		}
	}

	// Acorn image
	for (int h = 0; h < gAcornTexture.size();h++)
	{
		if( !gAcornTexture[h].loadFromFile( "acorn3.png", gRenderer ) ) // sprite sheet
		{
			std::cout << "Failed to load acorn!" << std::endl; // failed load
			success = false;
		}
	
		else
		{
			// acorn image
			gAcornClip[ 0 ].x = 111;
			gAcornClip[ 0 ].y = 64;
			gAcornClip[ 0 ].w = 18;
			gAcornClip[ 0 ].h = 18;

		}
	}
	
	gFont = TTF_OpenFont( "Caviar_Dreams_Bold.ttf", 22 ); //font for scoreboard

	if( gFont == NULL )
	{
		std::cout << "Failed to load lazy font! SDL_ttf Error: " << TTF_GetError() << std::endl;
		success = false;
	}
	else
	{
		//Render text
		SDL_Color textColor = { 0, 0, 0 };
		if( !gTitleTexture.loadFromRenderedText( "Shoot Like a Champion", textColor , gFont, gRenderer ))
		{
			std::cout << "Failed to render text texture!" << std::endl;
			success = false;
		}
		//string for score
		std::stringstream s;
		std::string points = "Score: ";
		s << points << score;
		std::string result = s.str();
		if( !gScoreTexture.loadFromRenderedText( result, textColor , gFont, gRenderer) )
		{
			std::cout << "Failed to render text texture!" << std::endl;
			success = false;
		}
		//string for level print out
		std::stringstream t;
		std::string level_on = "Level: ";
		t << level_on << currentLevel;
		std::string displayLevel = t.str();
		if( !gLevelTexture.loadFromRenderedText( displayLevel, textColor , gFont, gRenderer) )
		{
			std::cout << "Failed to render text texture!" << std::endl;
			success = false;
		}
		// string for nut number string
		std::stringstream v;
		std::string nut = "Nuts Left: ";
		v << nut << nutsLeft;
		std::string displaynut = v.str();
		if( !gNutTexture.loadFromRenderedText( displaynut, textColor , gFont, gRenderer) )
		{
			std::cout << "Failed to render text texture!" << std::endl;
			success = false;
		}
	}
	return success;
}

void close() //close funciton for game
{
	//Deallocate surface
	SDL_FreeSurface( gXOut );
	gXOut = NULL;

	for (int i = 0; i < gSquirrelSheetTexture.size();i++){
		gSquirrelSheetTexture[i].free();
	}

	for (int i = 0; i < gAcornTexture.size();i++){
		gAcornTexture[i].free();
	}

	gSpriteSheetTexture.free();

	// for font stuff
	gTitleTexture.free();
	TTF_CloseFont( gFont );
	gFont = NULL;

	SDL_DestroyRenderer( gRenderer );

	//Destroy window
	SDL_DestroyWindow( gWindow );
	gWindow = NULL;
	gRenderer = NULL;

	TTF_Quit(); // for font

	IMG_Quit();

	//Quit SDL subsystems
	SDL_Quit();
}

void checkCollision(SDL_Rect* squirrel, SDL_Rect* acorn, int squirrel_x, int squirrel_y, int acorn_x, int acorn_y, int k, int i) //collision detection for squirrel and acorn
{
	int leftS, leftL;
	int rightS, rightL;
	int topS, topL;
	int botS, botL;

	// sides of squirrel
	leftS = squirrel_x;
	rightS = squirrel_x + squirrel->w;
	topS = squirrel_y;
	botS = squirrel_y + squirrel->h;

	// sides of leprechaun
	leftL = acorn_x;
	rightL = acorn_x + acorn->w;
	topL = acorn_y;
	botL = acorn_y + acorn->h;

	//If any of the sides from A are outside of B
    if( botS <= topL )
    {
        return;
    }

    if( topS >= botL )
    {
        return;
    }

    if( rightS <= leftL )
    {
        return;
    }

    if( leftS >= rightL )
    {
        return;
    }

    //If none of the sides from squirrel are outside acorn
    gSquirrelSheetTexture[k].setIsAlive(false);
    gSquirrelSheetTexture[k].setYPos(1650);
    gSquirrelSheetTexture[k].setXPos(950);
    gAcornTexture[i].setIsAlive(false);
    gAcornTexture[i].setYPos(1650);
    gAcornTexture[i].setXPos(950);
    score +=20;
    return;	
}

void checkLepCollision(SDL_Rect* squirrel, SDL_Rect* leprechaun, int squirrel_x, int squirrel_y, int leprechaun_x, int leprechaun_y, int k) //collision detection of squirrel and leprechaun
{
	int leftS, leftL;
	int rightS, rightL;
	int topS, topL;
	int botS, botL;

	// sides of squirrel
	//hack to fixe collision detection depending on the number of enemies/current level
	if (currentLevel == 1){
		leftS = squirrel_x;
		rightS = squirrel_x + squirrel->w/1.6;
		topS = squirrel_y;
		botS = squirrel_y + squirrel->h/1.6;

		// sides of leprechaun
		leftL = leprechaun_x;
		rightL = leprechaun_x + leprechaun->w/1.3;
		topL = leprechaun_y;
		botL = leprechaun_y + leprechaun->h/1.3;
	}

	else {
		leftS = squirrel_x;
		rightS = squirrel_x + squirrel->w/1.9;
		topS = squirrel_y;
		botS = squirrel_y + squirrel->h/1.8;

		// sides of leprechaun
		leftL = leprechaun_x;
		rightL = leprechaun_x + leprechaun->w/1.6;
		topL = leprechaun_y;
		botL = leprechaun_y + leprechaun->h/1.2;
	}	
	
    if( botS <= topL )
    {
        return;
    }

    if( topS >= botL )
    {
        return;
    }

    if( rightS <= leftL )
    {
        return;
    }

    if( leftS >= rightL )
    {
        return;
    }

    //If none of the sides from squirrel are outside leprechaun
    quit = true;
    std::cout << "YOU LOST! LOSER." << std::endl;

    return;	
}


int main( int argc, char* args[] ) //main function
{
	int enemies_total=0; // number of enemies

	if( !init_menu() ) //initialize menu stuff
	{
		printf( "Failed to initialize!\n" );
	}
	else
	{
		//Load media
		if( !loadMedia_menu() )
		{
			printf( "Failed to load media!\n" );
		}
		else
		{	
			//Main loop flag
			bool quit_menu = false;

			//Event handler
			SDL_Event e_menu;

			//While application is running
			while( !quit_menu )
			{
				//Handle events on queue
				while( SDL_PollEvent( &e_menu ) != 0 )
				{
					//User requests quit_menu
					if( e_menu.type == SDL_QUIT )
					{
						quit_menu = true;
						quit= true;
					}

					if (e_menu.type == SDL_MOUSEMOTION) //keeps track of what item on menu is pressed to move on to the game or quit the program
					{
						int x = e_menu.motion.x; // mouse
						int y = e_menu.motion.y;

						if (x > ( ( SCREEN_WIDTH - LevelOneTexture.getWidth_menu() ) /2 ) && x < ( ( ( SCREEN_WIDTH - LevelOneTexture.getWidth_menu() ) /2 ) + LevelOneTexture.getWidth_menu() )
							&& (y > ( SCREEN_HEIGHT - LevelOneTexture.getHeight_menu() ) / 2.1 ) && y < ( ( ( SCREEN_HEIGHT - LevelOneTexture.getHeight_menu() ) / 2.1 ) + LevelOneTexture.getHeight_menu() ))
						{
							LevelOneTexture.loadFromRenderedText_menu( "Level One", {255,0,0}, gFont_menu, gRenderer );
						}
						else {
							LevelOneTexture.loadFromRenderedText_menu( "Level One", {0,0,0}, gFont_menu, gRenderer );
						}

						if (x > ( ( SCREEN_WIDTH - LevelTwoTexture.getWidth_menu() ) /2 ) && x < ( ( ( SCREEN_WIDTH - LevelTwoTexture.getWidth_menu() ) /2 ) + LevelTwoTexture.getWidth_menu() )
							&& (y > ( SCREEN_HEIGHT - LevelTwoTexture.getHeight_menu() ) / 1.8 ) && y < ( ( ( SCREEN_HEIGHT - LevelTwoTexture.getHeight_menu() ) / 1.8 ) + LevelTwoTexture.getHeight_menu() ))
						{
							LevelTwoTexture.loadFromRenderedText_menu( "Level Two", {255,0,0}, gFont_menu, gRenderer );
						}
						else {
							LevelTwoTexture.loadFromRenderedText_menu( "Level Two", {0,0,0}, gFont_menu, gRenderer );
						}

						if (x > ( ( SCREEN_WIDTH - LevelThreeTexture.getWidth_menu() ) /2 ) && x < ( ( ( SCREEN_WIDTH - LevelThreeTexture.getWidth_menu() ) /2 ) + LevelThreeTexture.getWidth_menu() )
							&& (y > ( SCREEN_HEIGHT - LevelThreeTexture.getHeight_menu() ) / 1.57 ) && y < ( ( ( SCREEN_HEIGHT - LevelThreeTexture.getHeight_menu() ) / 1.57 ) + LevelThreeTexture.getHeight_menu() ))
						{
							LevelThreeTexture.loadFromRenderedText_menu( "Level Three", {255,0,0}, gFont_menu, gRenderer );
						}
						else {
							LevelThreeTexture.loadFromRenderedText_menu( "Level Three", {0,0,0}, gFont_menu, gRenderer );
						}

						if (x > ( ( SCREEN_WIDTH - gQuitTexture.getWidth_menu() ) /2 ) && x < ( ( ( SCREEN_WIDTH - gQuitTexture.getWidth_menu() ) /2 ) + gQuitTexture.getWidth_menu() )
							&& (y > ( SCREEN_HEIGHT - gQuitTexture.getHeight_menu() ) / 1.4 ) && y < ( ( ( SCREEN_HEIGHT - gQuitTexture.getHeight_menu() ) / 1.4 ) + gQuitTexture.getHeight_menu() ))
						{
							gQuitTexture.loadFromRenderedText_menu( "Quit", {255,0,0} , gFont_menu, gRenderer );
						}
						else {
							gQuitTexture.loadFromRenderedText_menu( "Quit", {0,0,0}, gFont_menu, gRenderer );
						}
					}

					if (e_menu.type == SDL_MOUSEBUTTONDOWN) //checks release of mouse
					{
						if (e_menu.button.button = SDL_BUTTON_LEFT){ //left mouse buttom
							int xQuit = e_menu.button.x;
							int yQuit = e_menu.button.y;
							// sets the number of enemies to move onto the game or quit if player wants to 
							if ( xQuit > ( ( SCREEN_WIDTH - gQuitTexture.getWidth_menu() ) /2 ) && xQuit < ( ( ( SCREEN_WIDTH - gQuitTexture.getWidth_menu() ) /2 ) + gQuitTexture.getWidth_menu() )
							&& (yQuit > ( SCREEN_HEIGHT - gQuitTexture.getHeight_menu() ) / 1.4 ) && yQuit < ( ( ( SCREEN_HEIGHT - gQuitTexture.getHeight_menu() ) / 1.4 ) + gQuitTexture.getHeight_menu() )){
								quit = true;
								quit_menu = true;

							}
							if (xQuit > ( ( SCREEN_WIDTH - LevelOneTexture.getWidth_menu() ) /2 ) && xQuit < ( ( ( SCREEN_WIDTH - LevelOneTexture.getWidth_menu() ) /2 ) + LevelOneTexture.getWidth_menu() )
							&& (yQuit > ( SCREEN_HEIGHT - LevelOneTexture.getHeight_menu() ) / 2.1 ) && yQuit < ( ( ( SCREEN_HEIGHT - LevelOneTexture.getHeight_menu() ) / 2.1 ) + LevelOneTexture.getHeight_menu() ))
							{
								quit_menu = true;
								enemies_total = 6;
								currentLevel = 1;
							} 
							if (xQuit > ( ( SCREEN_WIDTH - LevelTwoTexture.getWidth_menu() ) /2 ) && xQuit < ( ( ( SCREEN_WIDTH - LevelTwoTexture.getWidth_menu() ) /2 ) + LevelTwoTexture.getWidth_menu() )
							&& (yQuit > ( SCREEN_HEIGHT - LevelTwoTexture.getHeight_menu() ) / 1.8 ) && yQuit < ( ( ( SCREEN_HEIGHT - LevelTwoTexture.getHeight_menu() ) / 1.8 ) + LevelTwoTexture.getHeight_menu() ))
							{
								quit_menu = true;
								enemies_total = 12;
								currentLevel = 2;
							} 
							if (xQuit > ( ( SCREEN_WIDTH - LevelThreeTexture.getWidth_menu() ) /2 ) && xQuit < ( ( ( SCREEN_WIDTH - LevelThreeTexture.getWidth_menu() ) /2 ) + LevelThreeTexture.getWidth_menu() )
							&& (yQuit > ( SCREEN_HEIGHT - LevelThreeTexture.getHeight_menu() ) / 1.57 ) && yQuit < ( ( ( SCREEN_HEIGHT - LevelThreeTexture.getHeight_menu() ) / 1.57 ) + LevelThreeTexture.getHeight_menu() ))
							{
								quit_menu = true;
								enemies_total = 25;
								currentLevel = 3;
							} 
						}
					}
				}

				//Clear screen
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
				SDL_RenderClear( gRenderer );

				//render_menu current frame
				gTitleTexture_menu.render_menu( ( SCREEN_WIDTH - gTitleTexture_menu.getWidth_menu() ) / 2, ( SCREEN_HEIGHT - gTitleTexture_menu.getHeight_menu() ) / 8 , NULL, 0.0, NULL, SDL_FLIP_NONE, gRenderer);
				LevelOneTexture.render_menu( ( SCREEN_WIDTH - LevelOneTexture.getWidth_menu() ) / 2, ( SCREEN_HEIGHT - LevelOneTexture.getHeight_menu() ) / 2.1, NULL, 0.0, NULL, SDL_FLIP_NONE, gRenderer );
				LevelTwoTexture.render_menu( ( SCREEN_WIDTH - LevelTwoTexture.getWidth_menu() ) / 2, ( SCREEN_HEIGHT - LevelTwoTexture.getHeight_menu() ) / 1.8, NULL, 0.0, NULL, SDL_FLIP_NONE, gRenderer );				
				LevelThreeTexture.render_menu( ( SCREEN_WIDTH - LevelThreeTexture.getWidth_menu() ) / 2, ( SCREEN_HEIGHT - LevelThreeTexture.getHeight_menu() ) / 1.57, NULL, 0.0, NULL, SDL_FLIP_NONE, gRenderer );				
				gQuitTexture.render_menu( ( SCREEN_WIDTH - gQuitTexture.getWidth_menu() ) / 2, ( SCREEN_HEIGHT - gQuitTexture.getHeight_menu() ) / 1.4, NULL, 0.0, NULL, SDL_FLIP_NONE, gRenderer);

				//Update screen
				SDL_RenderPresent( gRenderer );
			}
		}
	}

	//free_menu resources and close_menu SDL
	close_menu();	

	// For the GAME:

	int x = 0; // change in the x
	int y = 0; // change in the y
	int xposition, yposition; //position of leprechaun
	bool isMoving = false; //bool to detemrine if leprechaun should be moving
	int AcornAlive = 0; // number of acorns on the game field
	
	//string for score
	std::stringstream s;
	std::string points = "Score: ";

	nutsLeft = enemies_total*2; // initilizes nut count

	srand(time(0)); // for random
	int y_squir = 0; // change in the y

	//vecotor for squirrel class objects initialized
	for (int j = 0; j<enemies_total;j++){
		Squirrel temp;
		gSquirrelSheetTexture.push_back(temp);
	}

	//vector for acorn number initialized
	for (int j = 0; j<enemies_total*2;j++){
		Acorn temp;
		gAcornTexture.push_back(temp);
	}

	//Start up SDL and create window
	if( !init() )
	{
		std::cout << "Failed to initialize!" << std::endl;
	}
	else
	{
		//Load media
		if( !loadMedia() )
		{
			std::cout << "Failed to load media!" << std::endl;
		}
		else
		{			
			//Event handler
			SDL_Event e;

			int frame = 0; // starts at the first clip
			int frame_squir = 0;

			//While application is running
			while( !quit )
			{
				SDL_Delay(1);		
				//Handle events on queue
				SDL_PollEvent( &e );

				switch(e.type)
				{
					case SDL_QUIT:
						quit = true;
						break;
					case SDL_KEYDOWN:
						switch(e.key.keysym.sym) // if key pressed, leprechaun apprears to be running
						{
							case SDLK_LEFT:  x--; isMoving = true; break;
			                case SDLK_RIGHT: x++; isMoving = true; break;
			                case SDLK_UP:    y--; isMoving = true; break;
			                case SDLK_SPACE: AcornAlive++; if (nutsLeft>0)--nutsLeft; break;
						}
						break;
					case SDL_KEYUP:
						switch(e.key.keysym.sym) //if key released, leprechaun stops looking like its running
						{
							case SDLK_LEFT:  isMoving = false; break;
			                case SDLK_RIGHT: isMoving = false; break;
			                case SDLK_UP:    isMoving = false; break;
						}
						break;
				}

				//Apply the image
				SDL_BlitSurface( gXOut, NULL, gScreenSurface, NULL );
			
				//for leprechaun sprite sheet
				SDL_Rect* currentClip = &gSpriteClips[ frame / 4 ];
				xposition = (( SCREEN_WIDTH - currentClip->w ) / 2) + x;
				yposition = (9.75 * ( SCREEN_HEIGHT - currentClip->h ) / 10) + y;

				// for squirrel
				SDL_Rect* currentClipSquir = &gSquirrelClips[ frame_squir / 4 ];
				++y_squir;

				//for acorn
				SDL_Rect* currentClipAcorn = & gAcornClip[ 0 ];

				// checks to make sure leprechaun stays within the road
				if (xposition >= 550)
				{
					xposition = 550;
					x = 180;
				}
				if (xposition <= 285) 
				{
					xposition = 285;
					x = -81;
				}

				if (yposition <= 7) //checks to see if player won or lost depending on how many squirrels went passed the leprechan without being killed
				{
					bool check = true;
					for (int w=0; w <gSquirrelSheetTexture.size();w++){
						if (gSquirrelSheetTexture[w].getIsAlive()){
							check = false;
							std::cout <<"you lose" << std::endl;
							quit = true;
							break;
						}
					}
					if (check){
						std::cout << "you win!" << std::endl;
						quit = true;
					}
					yposition = 7;
				}

				//initializes position of squirrels by going through the vector elements
				for (int k=0; k<gSquirrelSheetTexture.size();k++){
					gSquirrelSheetTexture[k].setYPos(y_squir);
				}
				
				//checks each squirrel position if its out of range
				for ( int k=0; k< gSquirrelSheetTexture.size(); k++){
					if ( (gSquirrelSheetTexture[k].getYPos() >= 730)  )
					{
						gSquirrelSheetTexture[k].setYPos(1650);

					}
					// checks to see if acoern and squirrel are both alive; if yes collion function is called
					for (int i =0; i <gAcornTexture.size(); i++)
					{
						if (gAcornTexture[i].getIsAlive()) 
						{
							if (gSquirrelSheetTexture[k].getIsAlive())
							{
								checkCollision(currentClipSquir, currentClipAcorn, gSquirrelSheetTexture[k].getXPos() , gSquirrelSheetTexture[k].getYPos() , gAcornTexture[i].getXPos() , gAcornTexture[i].getYPos(), k, i);
							}
						}
					}
				}

				if (AcornAlive) // moves the acorn if there are acorns alive on the field
				{
					for ( int k=0; k< AcornAlive; k++){ 
						if (k<gAcornTexture.size()){
							gAcornTexture[k].setMove(5);
							gAcornTexture[k].setXPos(xposition);
							gAcornTexture[k].setYPos(yposition);
							if (gAcornTexture[k].getIsAlive())
								gAcornTexture[k].render(gAcornTexture[k].getXPos() , gAcornTexture[k].getYPos() , currentClipAcorn, gRenderer);
						}
					}
				}

				if ( isMoving ) //keeps changing the clip of leprechaun if key pressed
				{
					gSpriteSheetTexture.render( xposition, yposition , currentClip );
				}

				else // uses same clip if key not pressed
				{
					frame = 1;
					SDL_Rect* currentClip = &gSpriteClips[ frame / 4 ];

					gSpriteSheetTexture.render( xposition , yposition , currentClip );
				}

				// collision detection between squirrels and leprechaun
				for ( int k=0; k<gSquirrelSheetTexture.size(); k++)
				{
					checkLepCollision(currentClipSquir, currentClip, gSquirrelSheetTexture[k].getXPos() , gSquirrelSheetTexture[k].getYPos() , xposition , yposition , k);
					if ( gSquirrelSheetTexture[k].getIsAlive() )
						gSquirrelSheetTexture[k].render( gSquirrelSheetTexture[k].getXPos() , gSquirrelSheetTexture[k].getYPos() , currentClipSquir , gRenderer);
				}
				
				//text for the scoreboard
				std::stringstream s;
				std::string points = "Score: ";
				s << points << score;
				std::string result = s.str();

				std::stringstream t;
				std::string level_on = "Level: ";
				t << level_on << currentLevel;
				std::string displayLevel = t.str();

				std::stringstream v;
				std::string nut = "Nuts Left: ";
				v << nut << nutsLeft;
				std::string displaynut = v.str();

				gScoreTexture.free();
				gScoreTexture.loadFromRenderedText( result, {0,0,0} , gFont, gRenderer);

				gLevelTexture.free();
				gLevelTexture.loadFromRenderedText(displayLevel, {0,0,0}, gFont, gRenderer);

				gNutTexture.free();
				gNutTexture.loadFromRenderedText(displaynut, {0,0,0}, gFont, gRenderer);

				// for font:
				gTitleTexture.render( 10 , 10 , gRenderer);
				gScoreTexture.render( 10 , 50 , gRenderer);
				gLevelTexture.render( 10, 30, gRenderer);
				gNutTexture.render( 10, 70, gRenderer);

				SDL_RenderPresent( gRenderer );

				++frame; // moves onto next frame of leprechaun 
				++frame_squir; //moves clip of squirrel

				//reset the clip number if it exceedes the number of clips for available
				if( frame / 4 >= WALKING_ANIMATION_FRAMES ) 
				{
					frame = 0;
				}
				if( frame_squir / 4 >= SQUIRREL_ANIMATION_FRAMES ) 
				{
					frame_squir = 0;
				}
			}
		}
	}
	close(); //Free resources and close SDL

	return 0;
}