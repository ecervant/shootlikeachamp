#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <SDL2/SDL_ttf.h>

class Scoreboard
{
	public:
		Scoreboard();
		~Scoreboard();
		//Creates image from font string
		bool loadFromRenderedText( std::string textureText, SDL_Color textColor , TTF_Font *gFont = NULL , SDL_Renderer* gRenderer = NULL);

		//Deallocates texture
		void free();

		void setScore(int);

		int getScore();

		void render( int x, int y, SDL_Renderer* gRenderer = NULL, SDL_Rect* clip = NULL, double angle = 0.0, SDL_Point* center = NULL, SDL_RendererFlip flip = SDL_FLIP_NONE);


		//Gets image dimensions
		int getWidth();
		int getHeight();

	private:
		int score;
		SDL_Texture* scoreTexture;

		//Image dimensions
		int sWidth;
		int sHeight;
};

Scoreboard::Scoreboard(){
	score = 0;
	scoreTexture = NULL;
}

Scoreboard::~Scoreboard(){
	free();
}

bool Scoreboard::loadFromRenderedText( std::string textureText, SDL_Color textColor , TTF_Font *gFont, SDL_Renderer* gRenderer)
{
	//Get rid of preexisting texture
	free();

	//Render text surface
	SDL_Surface* textSurface = TTF_RenderText_Solid( gFont, textureText.c_str(), textColor );
	if( textSurface == NULL )
	{
		printf( "Unable to render text surface! SDL_ttf Error: %s\n", TTF_GetError() );
	}
	else
	{
		//Create texture from surface pixels
        scoreTexture = SDL_CreateTextureFromSurface( gRenderer, textSurface );
		if( scoreTexture == NULL )
		{
			printf( "Unable to create texture from rendered text! SDL Error: %s\n", SDL_GetError() );
		}
		else
		{
			//Get image dimensions
			sWidth = textSurface->w;
			sHeight = textSurface->h;
		}

		//Get rid of old surface
		SDL_FreeSurface( textSurface );
	}
	
	//Return success
	return scoreTexture != NULL;
}

void Scoreboard::free()
{
	//Free texture if it exists
	if( scoreTexture != NULL )
	{
		SDL_DestroyTexture( scoreTexture );
		scoreTexture = NULL;
		sWidth = 0;
		sHeight = 0;
	}
}

void Scoreboard::setScore(int x)
{
	score = x;
}

int Scoreboard::getScore()
{
	return score;
}

void Scoreboard::render( int x, int y, SDL_Renderer* gRenderer, SDL_Rect* clip, double angle, SDL_Point* center, SDL_RendererFlip flip )
{
	//Set rendering space and render to screen
	SDL_Rect renderQuad = { x, y, sWidth, sHeight };

	//Set clip rendering dimensions
	if( clip != NULL )
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	//Render to screen
	SDL_RenderCopyEx( gRenderer, scoreTexture, clip, &renderQuad, angle, center, flip );
}

int Scoreboard::getWidth()
{
	return sWidth;
}

int Scoreboard::getHeight()
{
	return sHeight;
}