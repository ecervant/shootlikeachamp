// Squirrel class
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>


class Squirrel
{
public:
	Squirrel();
	~Squirrel();

	bool loadFromFile(std::string path, SDL_Renderer* gRenderer);
	void free();

	void render( int x, int y, SDL_Rect* clip = NULL , SDL_Renderer* gRenderer = NULL);

	int getWidth();
	int getHeight();
	int getXPos();
	int getYPos();
	// void setXPos();
	int getRandom();
	//void setRandom(int num);
	void setYPos(int y);
	void setXPos(int x);
	bool getIsAlive();
	void setIsAlive(bool value);

private:
	SDL_Texture* sTexture;

	int sWidth;
	int sHeight;
	int squirrel_xpos;
	int squirrel_ypos;
	int random_y;
	bool isAlive;
};



Squirrel::Squirrel()
{

	sWidth = 0;
	sHeight = 0;
	sTexture = NULL;
	squirrel_xpos = rand() % 262 + 288; //between 288 and 550
	std::cout << squirrel_xpos << std::endl;
	squirrel_ypos = 0;
	random_y = rand() %200;
	isAlive = true;
}

Squirrel::~Squirrel()
{

	free();
}

bool Squirrel::loadFromFile( std::string path , SDL_Renderer* gRenderer)
{
	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );

	if (loadedSurface == NULL)
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;

	else
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);

		if (newTexture == NULL)
			std::cout << "Unable to create texture from " << path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		else
		{
			sWidth = loadedSurface->w;
			sHeight = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}

	sTexture = newTexture;
	return sTexture != NULL;
}

void Squirrel::free()
{
	if (sTexture != NULL)
	{
		SDL_DestroyTexture(sTexture);
		sTexture = NULL;
		sWidth = 0;
		sHeight = 0;
	}
}

void Squirrel::render(int x, int y, SDL_Rect* clip, SDL_Renderer* gRenderer)
{
	SDL_Rect renderQuad = { x, y, sWidth, sHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, sTexture, clip, &renderQuad ); 
}

int Squirrel::getWidth()
{
	return sWidth;
}

int Squirrel::getHeight()
{
	return sHeight;
}

int Squirrel::getXPos()
{
	return squirrel_xpos;
}

int Squirrel::getRandom(){
	return random_y;
}

// void Squirrel::setRandom(int num)
// {
// 	random_y = num;
// }

int Squirrel::getYPos()
{
	return squirrel_ypos;
}

void Squirrel::setXPos(int x)
{
 	squirrel_xpos =  x; // const
}

void Squirrel::setYPos(int y)
{
	squirrel_ypos = ( getHeight() / 10 ) + y- random_y;
}

bool Squirrel::getIsAlive()
{
	return isAlive;
}

void Squirrel::setIsAlive(bool value)
{
	isAlive = value;
}
