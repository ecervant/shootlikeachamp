// Acorn class
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>
#include <iterator>
#include <cstdlib>


class Acorn
{
public:
	Acorn();
	~Acorn();

	bool loadFromFile(std::string path, SDL_Renderer* gRenderer);
	void free();

	void render( int x, int y, SDL_Rect* clip = NULL , SDL_Renderer* gRenderer = NULL);

	int getWidth();
	int getHeight();
	int getXPos();
	int getYPos();
	int getRandom();
	int getMovement();
	void setMove(int);
	//void setRandom(int num);
	void setXPos(int x);
	void setYPos(int y);
	bool getIsAlive();
	void setIsAlive(bool value);


private:
	SDL_Texture* aTexture;

	int aWidth;
	int aHeight;
	int acorn_xpos;
	int acorn_ypos;
	int random_y;
	bool acorn_isAlive;
	int movement;
};



Acorn::Acorn()
{

	aWidth = 0;
	aHeight = 0;
	aTexture = NULL;
	// acorn_xpos = rand() % 262 + 288; //between 288 and 550
	acorn_xpos = -50;
	// std::cout << acorn_xpos << std::endl;
	acorn_ypos = -50;
	random_y = rand() %200;
	acorn_isAlive = true;
	movement = 5;
}

Acorn::~Acorn()
{

	free();
}

bool Acorn::loadFromFile( std::string path , SDL_Renderer* gRenderer)
{
	free();

	SDL_Texture* newTexture = NULL;

	SDL_Surface* loadedSurface = IMG_Load( path.c_str() );

	if (loadedSurface == NULL)
		std::cout << "Unable to load image " << path.c_str() << "! SDL_image Error: " << IMG_GetError() << std::endl;

	else
	{
		SDL_SetColorKey( loadedSurface, SDL_TRUE, SDL_MapRGB(loadedSurface->format, 0, 0xFF, 0xFF));
		newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);

		if (newTexture == NULL)
			std::cout << "Unable to create texture from " << path.c_str() << "! SDL Error: " << SDL_GetError() << std::endl;
		else
		{
			aWidth = loadedSurface->w;
			aHeight = loadedSurface->h;
		}

		SDL_FreeSurface(loadedSurface);
	}

	aTexture = newTexture;
	return aTexture != NULL;
}

void Acorn::free()
{
	if (aTexture != NULL)
	{
		SDL_DestroyTexture(aTexture);
		aTexture = NULL;
		aWidth = 0;
		aHeight = 0;
	}
}

void Acorn::render(int x, int y, SDL_Rect* clip, SDL_Renderer* gRenderer)
{
	SDL_Rect renderQuad = { x, y, aWidth, aHeight }; // rendering

 	if( clip != NULL ) // sets the dimentions of the image that pops up
	{
		renderQuad.w = clip->w;
		renderQuad.h = clip->h;
	}

	SDL_RenderCopy( gRenderer, aTexture, clip, &renderQuad ); 
}

int Acorn::getWidth()
{
	return aWidth;
}

int Acorn::getHeight()
{
	return aHeight;
}

int Acorn::getXPos()
{
	return acorn_xpos;
}

int Acorn::getRandom(){
	return random_y;
}

// void Acorn::setRandom(int num)
// {
// 	random_y = num;
// }

int Acorn::getYPos()
{
	return acorn_ypos;
}

void Acorn::setXPos(int x)
{
	acorn_xpos = x;
	// acorn_xpos =  SCREEN_WIDTH / 2 ; // const
}

void Acorn::setYPos(int y)
{
	acorn_ypos = y  - getMovement();
	// acorn_ypos = ( getHeight() / 10 ) + y- random_y;
}

bool Acorn::getIsAlive()
{
	return acorn_isAlive;
}

void Acorn::setIsAlive(bool value)
{
	acorn_isAlive = value;
}

int Acorn::getMovement()
{
	return movement;
}

void Acorn::setMove(int y)
{
	movement = movement + y;
}
